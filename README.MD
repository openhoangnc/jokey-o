JOKey - Bộ gõ tiếng Việt cho macOS

# Cài đặt / cập nhật JOKey

Thao tác này thay thế cho việc tải về và copy JOKey.app vào thư mục `~/Library/Input Methods/`

Chạy command sau trong Terminal
```
curl -sS https://afixer.app/jokey/install.sh | sh
```

- Nếu cài đặt mới, bạn cần [Thêm Jokey vào Input Sources](https://jokey-ime.blogspot.com/p/install.html)
- **Nếu là cập nhật, JOKey sẽ không thể hoạt động trên các app mà bạn đã mở trước khi chạy command trên, bạn cần thoát hoàn toàn app và mở lại.**
